import { wrap, Remote, createEndpoint, transfer } from "comlink";

import { NotImplementedError } from "../commons";
import { KernelWorkerBase, ExecuteResult } from "./kernel-worker";
import { KernelBase } from "../kernel";
import { ProxyFetch } from "./proxy";

// mock Worker to support sandboxing and keep webpack compatibility
if (globalThis.Worker.name !== "SandboxedWorker") {
  class SandboxedWorker extends Worker {
    constructor(url: string | URL, options?: any) {
      if (options.sandboxed) {
        const absURL = new URL(url).href;
        const assetsURL = absURL.substring(0, absURL.lastIndexOf("/"));
        // worker created with a 'data' url scheme has opaque origin
        // https://html.spec.whatwg.org/multipage/workers.html#script-settings-for-workers:concept-origin-opaque
        const workerScript = `self._assetsURL = "${assetsURL}";
           globalThis = {importScripts: self.importScripts, location: "${url}"};
           importScripts("${url}");
           globalThis = self;`.replace(/\s/g, "");
        super(`data:text/javascript;base64,${btoa(workerScript)}`);
      } else {
        super(url, options);
      }
    }
  }
  globalThis.Worker = SandboxedWorker;
}

export class KernelMainBase<Type extends KernelWorkerBase> extends KernelBase {
  // worker exposed through comlink
  private _worker?: Worker;
  private _remote?: Remote<Type>;

  constructor(options: any) {
    super(options);
  }

  /**
   * Is this kernel safe? (loaded in an isolated web worker)
   */
  public safeKernel(): boolean {
    return true;
  }

  /**
   * Get a new worker (should be overloaded).
   */
  protected newWorker(): Worker {
    throw new NotImplementedError("newWorker");
  }

  /**
   * Start the worker.
   */
  public async _start(): Promise<void> {
    await super._start();
    const worker = this.newWorker();

    // setup the proxy worker
    const proxyWorker = new Worker(
      new URL("./comlink-proxy.js", import.meta.url),
      {
        type: "module",
      }
    );
    const proxy = wrap<ProxyFetch>(proxyWorker);

    // setup the kernel worker
    this._worker = worker;
    worker.onmessage = (e) => this.processWorkerMessage(e.data);

    const KernelWorker = wrap<KernelWorkerBase>(worker);
    // why typescript complains about this call? yet, this is advertised in
    // https://github.com/GoogleChromeLabs/comlink/blob/dffe9050f63b1b39f30213adeb1dd4b9ed7d2594/docs/examples/03-classes-example/index.html#L15
    //@ts-ignore
    this._remote = await new KernelWorker(this._options);

    // bind ports
    const port = await proxy[createEndpoint]();
    await this.remote?.setProxyPort(transfer(port, [port]));

    // init remote
    await this.remote?.init();
  }

  /**
   * Comlink remote getter.
   */
  protected get remote(): Remote<Type> | undefined {
    return this._remote;
  }

  /**
   * Stop the worker.
   */
  public async _stop(): Promise<void> {
    this._worker?.terminate();
    this._worker = undefined;
    this._remote = undefined;
    await super._stop();
  }

  /**
   * Process messages from worker (if any).
   */
  protected processWorkerMessage(msg: {
    data: any;
    type: string;
    content: any;
  }): void {
    switch (msg.type) {
      case "stream":
        const { stream, text } = msg.content as {
          stream: "stdout" | "stderr";
          text: string;
        };
        const dataEvent = this.clone(msg.data);
        dataEvent.stream = stream;
        dataEvent.content = text;
        this.dispatchEvent("eval.output", dataEvent);
        break;
      case "download":
        const { content, filename } = msg.content;
        this.download(content, filename);
        break;
      case "display":
        this.dispatchEvent("eval.display", msg.data);
        break;
      case "input":
        const { prompt, password, data, sab } = msg.content;
        (async () => {
          const res = (await this.inputAsync(prompt, password, data)) as string;
          const encodedRes = new TextEncoder().encode(res);
          new Uint8Array(sab).set(encodedRes.subarray(0, sab.byteLength));
          Atomics.notify(new Int32Array(sab), 0, 1);
        })();
        break;
      default: // ignoring (probably a comlink message)
    }
  }

  public async evalAsync(
    code: string,
    outCallback: (_: string) => void,
    errCallback: (_: string) => void,
    data: any = null
  ): Promise<any> {
    // force interactivity in all modes
    data.interactive = true;

    this._execution_count++;

    // evaluation
    const execResult = (await this.remote?.eval(data, code)) as ExecuteResult;

    // return result
    let result;
    switch (execResult?.status) {
      case "ok":
        result = execResult.result;
        break;
      case "error":
        break;
    }
    return [result, this._execution_count];
  }

  /**
   * Put a file on the local (emulated) filesystem.
   */
  public async putFile(filename: string, content: ArrayBuffer) {
    await super.putFile(filename, content);
    await this.remote?.putFile(filename, content);
  }

  /**
   * Put an importable module on the local (emulated) filesystem
   * and load dependencies.
   */
  public async putModule(filename: string, content: ArrayBuffer) {
    await super.putModule(filename, content);
    await this.remote?.putModule(filename, content);
  }

  /**
   * Complete a code at the end (usefull for tab completion).
   *
   * Returns an array of two elements: the list of completions
   * and the start index.
   */
  public async complete(code: string): Promise<[string[], number] | []> {
    return (await this.remote?.complete(code)) ?? [];
  }

  /**
   * Tell wether we should wait for more code or if it can
   * be run as is.
   *
   * Useful to set ps1/ps2 in console prompt.
   */
  public async more(code: string): Promise<boolean> {
    return (await this.remote?.more(code)) ?? false;
  }
}
