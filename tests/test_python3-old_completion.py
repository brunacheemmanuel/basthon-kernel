def test_completion(selenium_py3old):
    assert selenium_py3old.run_js("return Basthon.complete('print.__d')") == [
        ["print.__delattr__(", "print.__dir__(", "print.__doc__"],
        0,
    ]
