FROM casatir/basthon-kernel:test-amd64

ENV DOCKER_IMAGE="test-amd64-recent"

# Get firefox
RUN mkdir -p /opt/
RUN wget -qO- "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=fr" | tar xjC /opt/
ENV PATH="/opt/firefox:${PATH}"

# Get Chromium
RUN mkdir -p /opt/
RUN wget -qO- "https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Linux_x64%2F$(curl -s -S "https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Linux_x64%2FLAST_CHANGE?alt=media")%2Fchrome-linux.zip?alt=media" | bsdtar -xvf- -C /opt/
RUN mv /opt/chrome-linux/ /opt/chromium/
RUN chmod u+x /opt/chromium/chrome
RUN ln -s /opt/chromium/chrome /opt/chromium/chromium
ENV PATH="/opt/chromium:${PATH}"

# Get Edge
RUN apt-get update
RUN apt-get install -y --no-install-recommends microsoft-edge-stable=114.0.1823.82-1
RUN rm -rf /var/lib/apt/lists/*
RUN wget -qO- https://msedgedriver.azureedge.net/$(microsoft-edge --version | cut -f3 -d' ')/edgedriver_linux64.zip | bsdtar -xvf- -C /opt/edge/
RUN chmod u+x /opt/edge/msedgedriver
ENV PATH="/opt/edge:${PATH}"

RUN echo 'PS1="\[\033[1;36m\](🐳 ${DOCKER_IMAGE})\[\033[0m\][\u@\h \W]\$ "' >> /etc/bash.bashrc
