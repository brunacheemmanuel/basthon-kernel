import { expose } from "comlink";
import { SQLKernelWorker } from "./worker";

declare let self: DedicatedWorkerGlobalScope;

expose(SQLKernelWorker);
