"""
Some usefull stuff.
"""

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ["OrderedSet"]


class OrderedSet(dict):
    """
    A set preserving insertion order based on a dict
    (since as of Python 3.7, dicts preserve order).

    Implementation is not intended to be efficient since it
    will be used with very few elements in Basthon.
    """
    def __init__(self, *args, **kwargs):
        if kwargs:
            raise ValueError("Named arguments not allowed.")
        super().__init__()
        self.update(*args)

    def add(self, elem):
        self[elem] = None

    def remove(self, elem):
        del self[elem]

    def discard(self, elem):
        dict.pop(self, elem, None)

    def pop(self):
        return self.popitem()[0]

    def copy(self):
        return OrderedSet(self)

    def update(self, *others):
        for a in others:
            for k in a:
                self.add(k)

    def union(self, *others):
        return OrderedSet(self, *others)

    def difference_update(self, *others):
        for o in others:
            for k in o:
                self.discard(k)

    def difference(self, *others):
        res = self.copy()
        res.difference_update(*others)
        return res

    def symmetric_difference_update(self, other):
        for k in other:
            if k in self:
                self.discard(k)
            else:
                self.add(k)

    def symmetric_difference(self, other):
        res = self.copy()
        res.symmetric_difference_update(other)
        return res

    def intersection_update(self, *others):
        self.difference_update(*([k for k in self if k not in o]
                                 for o in others))

    def intersection(self, *others):
        res = self.copy()
        res.intersection_update(*others)
        return res

    def __and__(self, other):
        return self.intersection(other)

    def __xor__(self, other):
        return self.symmetric_difference(other)

    def __or__(self, other):
        return self.union(other)

    def __sub__(self, other):
        return self.difference(other)

    def __reversed__(self):
        return OrderedSet(dict.__reversed__(self))

    def __repr__(self):
        return f"{{{', '.join(str(k) for k in self)}}}"

    def __str__(self):
        return repr(self)
