import objectInspect from "object-inspect";
import { KernelWorkerBase } from "@basthon/kernel-base/worker";

declare let self: DedicatedWorkerGlobalScope;

declare global {
  interface DedicatedWorkerGlobalScope {
    jsoo_runtime?: any;
    window?: any;
    Canvas: typeof OffscreenCanvas;
  }
}

export class JavaScriptKernelWorker extends KernelWorkerBase {
  constructor(options?: any) {
    // do not forget to call the parent constructor
    super(options);

    // the window object is not available in a webworker...
    self.window = self;
    self.Canvas = globalThis.OffscreenCanvas;
  }

  /*
   * Initialize the kernel.
   */
  protected async _init(): Promise<void> {
    console.info = (...args) => console.log(...args);
    console.warn = (...args) => console.error(...args);
    globalThis.addEventListener("error", (e: Event) =>
      console.error(e.toString())
    );
  }

  protected async _eval(data: any, code: string): Promise<any> {
    console.log = (...args: any[]) => {
      this.sendStdoutStream(data, args.join(" ") + "\n");
    };

    console.error = (...args: any[]) => {
      this.sendStderrStream(data, args.join(" ") + "\n");
    };

    const result = await globalThis.eval(code); // if a promise is returned, wait for it
    return typeof result === "undefined"
      ? undefined
      : { "text/plain": objectInspect(result) };
  }

  public putFile(filename: string, content: ArrayBuffer) {
    console.error(
      `Fichier ${filename} not added since putFile has no mean in the JS context.`
    );
  }

  public async putModule(filename: string, content: ArrayBuffer) {
    content = new Uint8Array(content);
    const ext = filename.split(".").pop();
    switch (ext) {
      case "js":
        let decoder = new TextDecoder("utf-8");
        const _content = decoder.decode(content);
        await globalThis.eval(_content);
        break;
      default:
        throw { message: "Only '.js' files supported." };
    }
  }

  public async complete(code: string): Promise<[string[], number] | []> {
    // basic completion based on global object
    const vars = Object.getOwnPropertyNames(self);
    const words = code.match(/(\w+)$/) ?? [];
    const word = words[0] ?? "";
    const matches = vars.filter((v) => v.startsWith(word));
    return [matches, code.length - word.length];
  }

  public async more(code: string): Promise<boolean> {
    return false;
  }
}
