import { KernelMainBase } from "@basthon/kernel-base/worker";
import { OCamlKernelWorker } from "./worker";

/**
 * An OCaml kernel that satisfies Basthon's API.
 */
export class OCamlKernel extends KernelMainBase<OCamlKernelWorker> {
  constructor(options: any) {
    super(options);
  }

  protected newWorker(): Worker {
    return new Worker(new URL("./comlink-worker.js", import.meta.url), {
      type: "module",
      //@ts-ignore
      sandboxed: true, // removing this line will cause security issues
    });
  }

  public language() {
    return "ocaml";
  }
  public languageName() {
    return "OCaml";
  }
  public moduleExts() {
    return ["ml"];
  }

  /**
   * List modules launched via putModule.
   */
  public userModules() {
    return [];
  }

  /**
   * Download a file from the VFS.
   */
  public getFile(path: string): Uint8Array {
    return new Uint8Array([]);
  }

  /**
   * Download a user module file.
   */
  public getUserModuleFile(filename: string): Uint8Array {
    return new Uint8Array([]);
  }

  /**
   * Mimic the OCaml's REPL banner.
   */
  public banner() {
    return "        OCaml version 4.11.1\n";
  }

  public ps1(): string {
    return "# ";
  }

  public ps2(): string {
    return "  ";
  }
}
