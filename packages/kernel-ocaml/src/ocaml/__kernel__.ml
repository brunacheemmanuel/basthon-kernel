(* __kernel__.ml *)

open Js_of_ocaml
open Js_of_ocaml_toplevel


(* Don't realy know what is this and if it's really useful *)
module Ppx_support = struct
  let init () = Ast_mapper.register "js_of_ocaml" (fun _ -> Ppx_js.mapper)
end


(* An object to dynamically set std streams callbacks *)
let io = object%js
  val stdout = (Js.Unsafe.pure_js_expr "console")##.log
  val stderr = (Js.Unsafe.pure_js_expr "console")##.error
end

let basthon =
  Js.Unsafe.pure_js_expr "self.basthon"

(* Dynamically load a module *)
let loadmodule path =
  Topdirs.dir_directory (Filename.dirname path);
  Toploop.mod_use_file Format.std_formatter path

(* Download a file form the VFS (to be called from the inner toplevel) *)
let download (path : string) : unit =
  let ch = open_in_bin path in
  let n = (in_channel_length ch) in
  let buf = Bytes.create n in
    really_input ch buf 0 n;
    close_in ch;
    ignore (basthon##ocamlDownload buf (Js.string (Filename.basename path)))


(* Create a new canvas (to be called from the inner toplevel) *)
let create_canvas () =
  Js.Unsafe.new_obj (Js.Unsafe.pure_js_expr "OffscreenCanvas") [|(Js.Unsafe.eval_string "0"); (Js.Unsafe.eval_string "0")|]


(* Save the canvas to the local filesystem as PNG (to be called from the inner toplevel) *)
let save_canvas canvas path : unit =
  let ext = String.lowercase_ascii (Filename.extension path) in
    if ext <> ".png" && ext <> ".jpg" && ext <> ".jpeg" then
      failwith "Only PNG/JPG files are supported!";
    ignore (basthon##saveCanvas canvas (Js.string path))


(* Send a display event to display a PNG image from the FS (to be called from the inner toplevel) *)
let display_image path : unit =
  if (String.lowercase_ascii (Filename.extension path)) <> ".png" then
    failwith "Only PNG files are supported!";
  let ch = open_in_bin path in
  let n = (in_channel_length ch) in
  let buf = Bytes.create n in
    really_input ch buf 0 n;
    close_in ch;
    ignore (basthon##displayImage buf)


(* Get the OCaml Basthon's version (to be called from the inner toplevel) *)
let version () = "0.0.1"


(* Main buffer to store exec results *)
let buffer = Buffer.create 100
let formatter = Format.formatter_of_buffer buffer


(* Main function to execute OCaml piece of code *)
let exec code =
  Buffer.clear buffer;
  let src = Js.to_string code in
  JsooTop.execute true formatter (src ^ ";;");
  Js.string (Buffer.contents buffer)


(* Initialise the OCaml toplevel *)
let init () =
  JsooTop.initialize ();
  Ppx_support.init ();
  Sys.interactive := false;
  (* add cwd to search path *)
  Topdirs.dir_directory "/static/";
  Sys.interactive := true;
  (* stdout/stderr flushers are io.stdout and io.stderr
   * so that they can easilly be set from JS *)
  Sys_js.set_channel_flusher stdout (fun str -> Js.Unsafe.meth_call io "stdout" [|Js.Unsafe.inject (Js.string str)|]);
  Sys_js.set_channel_flusher stderr (fun str -> Js.Unsafe.meth_call io "stderr" [|Js.Unsafe.inject (Js.string str)|]);
  (* stdin *)
  let readline () =
    Js.Opt.case
      (Dom_html.window##prompt (Js.string "OCaml expects input: ") (Js.string ""))
      (fun () -> "\n")
      (fun s -> Js.to_string s ^ "\n")
  in
  Sys_js.set_channel_filler stdin readline


(* What we export to JS *)
let () =
  Js.export "__kernel__" (object%js
      val init = init
      val exec = exec
      val io = io
      val createfile = Js.wrap_meth_callback
          (fun _ name content -> Sys_js.update_file ~name:(Js.to_string name) ~content:(Typed_array.String.of_arrayBuffer content))
      val loadmodule = Js.wrap_meth_callback
          (fun _ path -> loadmodule (Js.to_string path))
      val download = download
      val createcanvas = create_canvas
      val savecanvas = save_canvas
      val displayimage = display_image
      val version = version
    end)
