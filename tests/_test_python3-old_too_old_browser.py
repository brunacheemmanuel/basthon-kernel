from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import selenium.webdriver.support.ui as ui
import pytest


def test_main(selenium_py3old):
    """
    We test the presence of an error message for old version of Firefox.
    We test against FF 56 since it is the first version to support headless
    selenium.
    """
    driver = selenium_py3old.driver
    browser = driver.capabilities["browserName"]
    if browser != "firefox":
        pytest.skip("test not supported")
    xpath = "//div[contains(., 'Erreur de chargement de Basthon !!!') and contains(., 'Vérifiez que votre navigateur est à jour.') and contains(., 'Version détectée : Firefox 56.')]"
    query = (By.XPATH, xpath)
    wait = ui.WebDriverWait(driver, 10)
    wait.until(ec.visibility_of_element_located(query))
