import { Remote } from "comlink";
import { ProxyFetch } from "./worker/proxy";

declare global {
  interface Window {
    _remoteProxy: Remote<ProxyFetch>;
    _assetsURL?: string;
  }
}

/**
 * An error thrown by not implemented API functions.
 */
class NotImplementedError extends Error {
  constructor(funcName: string) {
    super(`Function ${funcName} not implemented!`);
    this.name = "NotImplementedError";
  }
}

/* fetch from local FS */
const fetchFromLocalFS = async (
  localfsScheme: string,
  url: string
): Promise<Response> => {
  const toDataURL = async (content: Uint8Array): Promise<string> => {
    return await new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        resolve(reader.result as string);
      };
      reader.onerror = reject;
      reader.readAsDataURL(new Blob([content]));
    });
  };
  const prefix = localfsScheme;
  const path = url.slice(prefix.length);
  let content = null;
  const options = {
    status: 200,
    statusText: "OK",
    headers: new Headers(),
  };
  try {
    //@ts-ignore
    content = self.Basthon.getFile(path);
    const dataURL = await toDataURL(content);
    const mime = dataURL.substring(
      dataURL.indexOf(":") + 1,
      dataURL.indexOf(";")
    );
    content = content.buffer;
    options.headers.append("Content-Type", mime);
    options.headers.append("Content-Length", content.byteLength);
  } catch (e) {
    options.status = 404;
    options.statusText = "Not Found";
  }
  return new Response(content, options);
};

/**
 * Get the origin of an url.
 */
const origin = (url: string): string => new URL(url).origin;

/**
 * Get the parent path of an url.
 */
const parent = (url: string): string => {
  const abs = new URL(url).href;
  return abs.substring(0, abs.lastIndexOf("/"));
};

/**
 * we mock the fetch function to:
 *  - redirect queries to local FS
 *  - bypass the worker's opaque origin
 */
const mockFetch = () => {
  // already mocked?
  if (self.fetch.name === "mockedFetch") return;
  const trueFetch = self.fetch;
  async function mockedFetch(
    request: string | Request | URL,
    init: RequestInit | undefined
  ): Promise<Response> {
    if (self.importScripts == null || self._assetsURL == null)
      return Response.error();
    // force all params to init object
    if (request instanceof Request) init = { ...request, ...init };
    // ensure credentials are not sent
    init = { ...init, ...{ credentials: "omit" } };
    const url = (request as Request).url ?? request.toString();

    const localfsScheme = "filesystem:/";
    try {
      // requests towards localfs
      if (url.startsWith(localfsScheme)) {
        return await fetchFromLocalFS(localfsScheme, url);
      } else if (origin(url) === origin(self._assetsURL)) {
        // requests towards same origin
        // only url under the assets folder are proxyied
        if (parent(url).startsWith(self._assetsURL)) {
          const proxy = self._remoteProxy;
          const { body, options } = await proxy.fetch(url, init);
          return new Response(body, options);
        }
        throw new Error("Ressource is outside the proxy' scope");
      }
      return await trueFetch(url, init);
    } catch (e: any) {
      console.error(
        `security proxy: request to ${url} throw error: ${e.toString()}`
      );
      return Response.error();
    }
  }
  self.fetch = mockedFetch;
};

export { NotImplementedError, mockFetch };
