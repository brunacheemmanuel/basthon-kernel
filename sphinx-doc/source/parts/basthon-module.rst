`basthon` API
=============

The `basthon` module
--------------------

.. automodule:: basthon
   :members:

The `basthon.autoeval` submodule
---------------------------------

.. automodule:: basthon.autoeval
   :show-inheritance:
   :members: validationclass, Validate, equality, ValidateSequences

.. autoclass:: ValidateVariables
   :show-inheritance:
.. autoclass:: ValidateFunction
   :show-inheritance:
.. autoclass:: ValidateFunctionPretty
   :show-inheritance:
