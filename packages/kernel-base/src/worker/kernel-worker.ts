import { PromiseDelegate } from "promise-delegate";
import { NotImplementedError, mockFetch } from "../commons";
import { wrap, Remote } from "comlink";
import { ProxyFetch } from "./proxy";

declare let self: DedicatedWorkerGlobalScope;

declare global {
  interface DedicatedWorkerGlobalScope {
    _remoteProxy: Remote<ProxyFetch>;
    basthon: KernelWorkerBase;
    _prompt: (p?: string, _d?: string) => string | null;
  }
}

type ExecuteResult = {
  data: any;
  status: "ok" | "error";
  result?: any;
  error?: any;
};

class KernelWorkerBase {
  private _ready = new PromiseDelegate<void>();
  protected __eval_data__: any;

  constructor(options?: any) {
    /* we mock the fetch function to:
     *  - redirect queries to local FS
     *  - bypass the worker's opaque origin
     */
    mockFetch();

    // useful for ffi
    self.basthon = this;

    // global prompt redirected to our input
    if (self._prompt == null) self._prompt = globalThis.prompt;
    globalThis.prompt = (prompt: string | undefined) => this.input(prompt);
  }

  /*
   * Initialize the kernel. Should be overloaded.
   */
  protected async _init(): Promise<void> {}

  /**
   * Initialize the kernel. It is called by initWorker in KernelBase.
   */
  public async init(): Promise<void> {
    try {
      await this._init();
      this._ready.resolve();
    } catch (e) {
      this._ready.reject(e);
    }
  }

  /**
   * Wait for the kernel to be ready.
   */
  public ready(): Promise<void> {
    return this._ready.promise;
  }

  /**
   * Set the comlink communication port to proxy fetch.
   */
  public setProxyPort(port: MessagePort): void {
    self._remoteProxy = wrap(port);
  }

  /**
   * Send stream to stdout/stderr.
   */
  private sendStream(data: any, stream: "stdout" | "stderr", text: string) {
    globalThis.postMessage({
      data,
      type: "stream",
      content: { stream, text },
    });
  }

  /**
   * Send stream to stdout.
   */
  protected sendStdoutStream(data: any, text: string): void {
    this.sendStream(data, "stdout", text);
  }
  /**
   * Send stream to stderr.
   */
  protected sendStderrStream(data: any, text: string): void {
    this.sendStream(data, "stderr", text);
  }

  protected async blobToDataURL(blob: Blob): Promise<string> {
    return await new Promise((resolve) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result as string);
      reader.readAsDataURL(blob);
    });
  }

  /**
   * Display a blob (only png images are supported).
   */
  protected async displayBlob(blob: Blob, data: any): Promise<void> {
    if (data == null) data = this.clone(this.__eval_data__);
    const dataURL = await this.blobToDataURL(blob);
    const pngPrefix = "data:image/png;base64,";
    if (dataURL.startsWith(pngPrefix)) {
      const png = dataURL.slice(pngPrefix.length);
      data["display_type"] = "multiple";
      data["content"] = { "image/png": png };
    } else {
      return;
    }
    globalThis.postMessage({ data, type: "display" });
  }

  /**
   * Display an element like a canvas, ... in the frontend.
   */
  public display(element: any): void {
    const data = this.clone(this.__eval_data__);
    if (
      globalThis.OffscreenCanvas != null &&
      element instanceof OffscreenCanvas
    ) {
      element
        .convertToBlob()
        .then((blob: Blob) => this.displayBlob(blob, data));
    } else {
      data["display_type"] = "multiple";
      data["content"] = element;
      globalThis.postMessage({ data, type: "display" });
    }
  }

  /**
   * Evaluate code and return an object with mime type keys and string value.
   * To be overloaded.
   */
  protected async _eval(
    data: any,
    code: string
  ): Promise<undefined | { [key: string]: string }> {
    throw new NotImplementedError("_eval");
  }

  /**
   * Evaluate a string of code. To be called by main thread (through comlink).
   */
  public async eval(data: any, code: string): Promise<ExecuteResult> {
    this.__eval_data__ = data;
    try {
      const result = await this._eval(data, code);
      return { data, status: "ok", result };
    } catch (e: any) {
      const { name, stack, message } = e as Error;
      this.sendStderrStream(data, e.toString());
      return {
        data,
        status: "error",
        error: {
          name: name,
          value: message,
          traceback: stack?.toString(),
        },
      };
    }
  }

  /**
   * Put a file on the local (emulated) filesystem.
   */
  public putFile(filename: string, content: ArrayBuffer) {
    throw new NotImplementedError("putFile");
  }

  /**
   * Put an importable module on the local (emulated) filesystem
   * and load dependencies.
   */
  public putModule(filename: string, content: ArrayBuffer) {
    throw new NotImplementedError("putModule");
  }

  /**
   * Complete the submited code.
   */
  public async complete(code: string): Promise<[string[], number] | []> {
    return [];
  }

  /**
   * Tell wether we should wait for more code or if it can
   * be run as is.
   *
   * Useful to set ps1/ps2 in console prompt.
   */
  public async more(code: string): Promise<boolean> {
    throw new NotImplementedError("more");
  }

  /**
   * Send a message to the main thread to download a file.
   */
  public download(content: Uint8Array | string, filename: string): void {
    globalThis.postMessage({
      type: "download",
      content: { content, filename },
    });
  }

  /**
   * Use JSON stringify/parse to copy an object.
   */
  protected clone(data: any) {
    return JSON.parse(JSON.stringify(data));
  }

  /**
   * Sync sleep for a certain duration in seconds.
   */
  public sleep(duration: number): void {
    duration = duration * 1000; // to milliseconds
    if (globalThis.crossOriginIsolated) {
      const sab = new Int32Array(new SharedArrayBuffer(4));
      Atomics.wait(sab, 0, 0, duration);
      return;
    } else {
      const t0 = Date.now();
      // active sleep
      while (Date.now() - t0 < duration);
    }
  }

  /**
   * Decode the string contained in a SharedArrayBuffer to
   * bypas this issue:
   * https://github.com/whatwg/encoding/issues/172
   */
  private decodeSharedArrayBuffer(sab: SharedArrayBuffer): string {
    const array = new Uint8Array(sab);
    let length = array.length;
    for (; length > 0 && array[length - 1] === 0; length--);
    if (length === 0) return "";
    const cropped = new Uint8Array(length);
    cropped.set(array.subarray(0, length));
    try {
      return new TextDecoder().decode(cropped);
    } catch (e) {
      if (array.length === length)
        throw new Error("String too long, can't decode");
      throw e;
    }
  }

  /**
   * Sync input that does not use the ugly prompt but send
   * an eval.input event tho the main thread and wait for the response.
   */
  public input(
    prompt: string | null | undefined,
    password: boolean = false
  ): string {
    if (globalThis.crossOriginIsolated) {
      const data = this.clone(this.__eval_data__);
      const sab = new SharedArrayBuffer(1024);
      globalThis.postMessage({
        type: "input",
        content: { data, prompt, password, sab },
      });
      Atomics.wait(new Int32Array(sab), 0, 0);
      return this.decodeSharedArrayBuffer(sab);
    } else {
      return self._prompt(prompt ?? "") ?? "";
    }
  }
}

export { ExecuteResult, KernelWorkerBase };
