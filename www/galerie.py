examples = {
    "Python 3": {
        "python3-courbes.py": {
            "desc": "Tracé de courbes avec Matplotlib",
        },
        "python3-project-euler.py": {
            "desc": "Un problème du <a href='https://projecteuler.net/'>Projet Euler</a>",
        },
        "python3-spirale.py": {
            "desc": "Une spirale avec Turtle",
        },
        "python3-3d-plot.ipynb": {
            "desc": "Tracé de surface avec Matplotlib",
        },
        "python3-sklearn.ipynb": {
            "desc": "Classification avec Scikit-Learn",
        },
        "python3-sympy.ipynb": {
            "desc": "Calcul formel avec Sympy",
        },
        "python3-decorateurs.ipynb": {
            "desc": "Fractales, Turtle et décorateurs",
        },
        "python3-pypi.ipynb": {
            "desc": "Chargement d'un module PyPi",
        },
        "python3-easter-eggs.ipynb": {
            "desc": "Quelques easter eggs de (C)Python",
        },
        "python3-markdown.ipynb": {
            "desc": "Utilisation de Markdown dans un notebook",
        },
        "python3-folium.py": {
            "desc": "Des cartes avec Folium",
        },
        "python3-folium-notebook.ipynb": {
            "desc": "Folium dans un notebook",
        },
        "python3-linear-regression.py": {
            "desc": "Régression linéaire",
        },
        "python3-lorenz.py": {
            "desc": "Attracteurs de Lorenz",
        },
        "python3-requests.py": {
            "desc": "Requêtes HTTP avec requests",
        },
        "python3-dom.ipynb": {
            "desc": "Modification du DOM",
        },
        "python3-pandas.ipynb": {
            "desc": "Utilisation de Pandas",
        },
        "python3-osm-process.ipynb": {
            "desc": "Récupération et traitement des données OpenStreetMap de la France",
        },
        "python3-contour-france.py": {
            "desc": "Contour de la France continentale (OSM)",
        },
        "python3-centre-france.ipynb": {
            "desc": "Où est le centre de la France ?",
        },
        "python3-turtle-writing.ipynb": {
            "desc": "Une tortue qui sait écrire",
        },
        "python3-lists.ipynb": {
            "desc": "Des listes chaînées bien typées",
        },
        "python3-graphviz-console.py": {
            "desc": "Graphviz dans la console",
        },
        "python3-graphviz-notebook.ipynb": {
            "desc": "Graphviz dans un notebook",
        },
        "python3-traitement-image.ipynb": {
            "desc": "Traitement d'image avec Numpy et Matplotlib",
            "aux": "examples/sandbox.png",
        },
        "python3-snake.ipynb": {
            "desc": "Traitement d'image avancé avec Scipy",
            "aux": "examples/snake.png",
        },
        "python3-p5-square.py": {
            "desc": "Un exemple simple d'utilisation de p5",
        },
        "python3-p5-basthon-on-fire.py": {
            "desc": "p5 enflamme Basthon",
        },
        "python3-p5-torus.py": {
            "desc": "Animation 3d avec p5",
        },
        "python3-p5-tree.py": {
            "desc": "Un arbre récursif avec p5",
        },
        "python3-p5-waves.py": {
            "desc": "De drôles de bestioles avec p5",
        },
        "python3-p5-camera.py": {
            "desc": "Utilisation de la webcam avec p5",
        },
        "python3-p5.ipynb": {
            "desc": "Utilisation de p5 dans un notebook",
        },
        "python3-p5-balls.ipynb": {
            "desc": "Des balles qui rebondissent avec p5",
        },
        "python3-p5-instance.ipynb": {
            "desc": "Plusieurs dessins avec p5 en mode instance",
        },
        "python3-slideshow.ipynb": {"desc": "Un diaporama avec un notebook (RISE) "},
        "python3-moon.ipynb": {
            "desc": "Manipulation d'images avec PIL",
            "aux": "examples/moon.jpg",
        },
        "python3-pythontutor-variables.py": {
            "desc": "Visualisation d'exécution avec PythonTutor (Console)",
        },
        "python3-pythontutor.ipynb": {
            "desc": "Visualisation d'exécution avec PythonTutor (Notebook)",
        },
        "python3-lotka-volterra.ipynb": {
            "desc": "Intégration d'EDO avec Scipy",
        },
        "python3-qrcode.ipynb": {
            "desc": "Des codes QR dans le notebook",
        },
        "python3-qrcode-strange.py": {
            "desc": "Un drôle de code QR...",
        },
        "python3-pyroutelib3.ipynb": {
            "desc": "Un calcul d'intinéraire avec Pyroutelib3",
        },
        "python3-chute-libre-simple.py": {
            "desc": "Étude simple d'une chute libre",
        },
        "python3-chute-libre.ipynb": {
            "desc": "Étude avancée d'une chute libre",
        },
        "python3-ipythonblocks.ipynb": {
            "desc": "Apprendre avec ipythonblocks",
        },
        "python3-lolviz.ipynb": {
            "desc": "Visualiser des structures de données avec lolviz",
        },
        "python3-binarytree.ipynb": {
            "desc": "Manipuler et visualiser des arbres binaires avec binarytree",
        },
        "python3-rcviz.ipynb": {
            "desc": "Visualiser des appels récursifs avec rcviz",
        },
        "python3-matplotlib-animation.ipynb": {
            "desc": "Des animations avec Matplotlib",
        },
        "python3-matplotlib-animation-console.py": {
            "desc": "Des animations Matplotlib dans la console",
        },
        "python3-audio.ipynb": {
            "desc": "Du son dans un notebook",
        },
        "python3-rupycube.ipynb": {
            "desc": "Un Rubik's Cube avec p5",
        },
        "python3-admonitions.ipynb": {
            "desc": "Markdown et l'extension 'admonitions'",
        },
        "python3-opencv.ipynb": {
            "desc": "Détection de visages avec opencv",
            "aux": [
                "examples/monty-python.jpg",
                "examples/haarcascade_frontalface_default.xml",
            ],
        },
        "python3-drawsvg.ipynb": {
            "desc": "Des SVG avec drawSvg",
        },
        "python3-plotly.ipynb": {
            "desc": "Plotly dans le notebook",
        },
        "python3-filesystem.ipynb": {
            "desc": "Utilisation des fichiers locaux",
            "aux": "kernelspecs/python3/logo-64x64.png",
        },
        "python3-sequenced.ipynb": {
            "desc": "Un notebook séquencé",
            "module": "examples/_validation.py",
            "extensions": ["sequenced"],
        },
        "python3-mocodo.ipynb": {"desc": "Mocodo dans un notebook"},
        "python3-mermaid.ipynb": {"desc": "De jolis schémas avec Mermaid"},
    },
    "SQL": {
        "sql-voitures.ipynb": {
            "desc": "Chargement d'une base depuis un fichier",
            "module": "examples/voitures.sql",
        },
        "sql-foreign-key.ipynb": {
            "desc": "Une clef étrangère",
        },
        "sql-export.ipynb": {
            "desc": "Export binaire d'une base",
        },
        "sql-dot-commands.ipynb": {
            "desc": "Commandes spéciales (dot commands)",
        },
    },
    "OCaml": {
        "ocaml-factorial.ml": {
            "desc": "Fonction factorielle",
        },
        "ocaml-fichiers.ipynb": {
            "desc": "Manipulation de fichiers texte",
            "aux": "examples/lorem-ipsum.txt",
        },
        "ocaml-hanoi.ml": {
            "desc": "Tours de Hanoï",
        },
        "ocaml-graphics.ipynb": {
            "desc": "Utilisation du module Graphics",
        },
        "ocaml-pong.ml": {
            "desc": "Animation sur un canvas",
        },
        "ocaml-module-externe.ipynb": {
            "desc": "Charger ses propres modules",
            "module": "examples/primes.ml",
        },
        "ocaml-fichiers-annexes.ml": {
            "desc": "Joindre des fichiers annexes",
            "aux": "examples/basthon.png",
        },
        "ocaml-files.ipynb": {
            "desc": "Écrire, lire et télécharger des fichiers",
        },
    },
    "JavaScript": {
        "javascript-showcase.ipynb": {"desc": "Un notebook d'exemples"},
        "javascript-syncasync.ipynb": {"desc": "Synchronicité et asynchronicité"},
    },
}

# to be removed after Python 3.10 is fully merged (and 3.8 removed)
py3 = examples["Python 3"]
del examples["Python 3"]
examples = {"Python 3": py3, "Python 3-Old": py3} | examples
