import { KernelLoader } from "@basthon/kernel-loader";

declare global {
  interface Window {
    basthonRoot?: string;
    basthonKernel?: string;
    basthonKernelLoader?: KernelLoader;
  }
}

const loader = (window.basthonKernelLoader = new KernelLoader({
  rootPath: window.basthonRoot ?? "./",
  language: window.basthonKernel ?? "python3",
}));

(async () => {
  loader.showLoader("Chargement de Basthon...", true);
  await loader.kernelStarted();
})();
