import { expose } from "comlink";
import { JavaScriptKernelWorker } from "./worker";

declare let self: DedicatedWorkerGlobalScope;

expose(JavaScriptKernelWorker);
