import { KernelWorkerBase } from "@basthon/kernel-base/worker";
import { Toplevel } from "./toplevel_types";

declare let self: DedicatedWorkerGlobalScope;

declare global {
  interface DedicatedWorkerGlobalScope {
    jsoo_runtime?: any;
    window?: any;
  }
}

export class OCamlKernelWorker extends KernelWorkerBase {
  private __kernel__?: Toplevel;
  private _initInnerCode: string;

  constructor(options: any) {
    // do not forget to call the parent constructor
    super(options);
    // the window object is not available in a webworker...
    self.window = self;

    this._initInnerCode = `\
open Js_of_ocaml
module Basthon = struct
  let download (path: string): unit = ignore((Js.Unsafe.eval_string "self.basthon.__kernel__")##download path)
  let sleep (duration: float): unit = ignore((Js.Unsafe.eval_string "self.basthon")##sleep duration)
  let create_canvas () = (Js.Unsafe.eval_string "self.basthon.__kernel__")##createcanvas()
  let display_canvas canvas: unit = ignore((Js.Unsafe.eval_string "self.basthon")##displayCanvas canvas)
  let save_canvas canvas (path: string): unit = ignore((Js.Unsafe.eval_string "self.basthon.__kernel__")##savecanvas canvas path)
  let download_canvas ?(format = "png") canvas: unit = ignore((Js.Unsafe.eval_string "self.basthon")##downloadCanvas canvas (Js.string format))
  let display_image (path: string): unit = ignore((Js.Unsafe.eval_string "self.basthon.__kernel__")##displayimage path)
  let version () : string = (Js.Unsafe.eval_string "self.basthon.__kernel__")##version()
  let help () : unit = print_endline {ext|\
Basthon module
  help:               Show this help.
  download path:      Download a file from the local filesystem.
  sleep duration:     Sleep for a certain amount of seconds.
  display_image path: Display a PNG image from the local filesystem.
  create_canvas:      Create a HTML5 canvas to be displayed with display_canvas.
  display_canvas canvas:   Display a HTML5 canvas created with create_canvas.
  save_canvas canvas path: Save a canvas to a PNG/JPG file to the local filesystem.
  download_canvas ?(format = "png") canvas: Download a canvas to a PNG/JPG file.
|ext}
end`;
  }

  /*
   * Initialize the kernel.
   */
  protected async _init(options?: any): Promise<void> {
    // io redirections
    console.info = (...args) => console.log(...args);
    console.warn = (...args) => console.error(...args);
    globalThis.addEventListener("error", (e: Event) =>
      console.error(e.toString())
    );

    // kernel loading
    const { __kernel__ } = await import("./__kernel__");
    this.__kernel__ = __kernel__;
    if (this.__kernel__?.init() !== 0)
      throw new Error("Can't start OCaml kernel!");

    // mock Graphics_js.draw_image for use in worker
    // see https://github.com/ocsigen/js_of_ocaml/blob/1c43da9a925a9df247548158879439ef4039eb38/runtime/graphics.js#L442
    self.jsoo_runtime.caml_gr_draw_image = (
      im: ImageData,
      x: number,
      y: number
    ) => {
      const s = self.jsoo_runtime.caml_gr_state_get();
      s.context.putImageData(im, x, s.height - im.height - y);
      return 0;
    };

    // execute magic init code
    this.__kernel__?.exec(this._initInnerCode);
  }

  protected async _eval(
    data: any,
    code: string
  ): Promise<{ "text/plain": string } | undefined> {
    if (this.__kernel__ == null) return;

    this.__eval_data__ = data;
    this.__kernel__.io.stdout = (...args: any[]) => {
      this.sendStdoutStream(data, args.join(" "));
    };
    this.__kernel__.io.stderr = (...args: any[]) => {
      this.sendStderrStream(data, args.join(" "));
    };

    const result: string | object | undefined = this.__kernel__?.exec(code);

    if (typeof result === "string" && result.length > 0)
      return { "text/plain": result.replace(/\n$/, "") };
    return undefined;
  }

  /**
   * Is the source ready to be evaluated or want we more?
   * Usefull to set ps1/ps2 in teminal prompt.
   */
  public async more(code: string): Promise<boolean> {
    return false;
  }

  /**
   * Put a file on the local (emulated) filesystem.
   */
  public putFile(filename: string, content: ArrayBuffer) {
    if (this.__kernel__ == null) return;
    this.__kernel__.createfile(filename, content);
  }

  /**
   * Put an importable module on the local (emulated) filesystem
   * and load dependencies.
   */
  public putModule(filename: string, content: ArrayBuffer) {
    this.putFile(filename, content);
    /* why is this needed?
     * even if path is already added using the #directory directive,
     * one should recall the directive each time the folder is modified...
     */
    this.__kernel__?.loadmodule(filename);
  }

  /**
   * OCaml wrapper arround Kernel.download (to be called by __kernel__.ml).
   */
  public ocamlDownload(content: any, filename: string) {
    const array = self.jsoo_runtime.caml_convert_bytes_to_array(content);
    this.download(array, filename);
  }

  /**
   * Send eval.display event with the given canvas
   * and ocaml-canvas as display_type.
   */
  public displayCanvas(canvas: OffscreenCanvas) {
    this.display(canvas);
  }

  /**
   * Save a canvas to a file on the local FS.
   */
  public async saveCanvas(canvas: OffscreenCanvas, path: string) {
    if (this.__kernel__ == null) return;
    const ext = path.split(".").pop()?.toLowerCase();
    let mime = "image/png";
    if (ext === "jpg" || ext === "jpeg") mime = "image/jpeg";
    const blob = await canvas.convertToBlob({ type: mime });
    const content = await blob.arrayBuffer();
    this.__kernel__.createfile(path, content);
  }

  /**
   * Download a canvas as an image file (png or jpg).
   */
  public downloadCanvas(canvas: OffscreenCanvas, format: "png" | "jpg") {
    if (format == null) format = "png";
    //@ts-ignore
    format = format.toString().toLowerCase();
    const types = { jpg: "image/jpeg", jpeg: "images/jpeg", png: "image/png" };
    if (!(format in types)) format = "png";
    const mime = types[format] as string;
    (async () => {
      const blob = await canvas.convertToBlob({ type: mime });
      let image = await this.blobToDataURL(blob);
      // image = image.replace(mime, "image/octet-stream");
      this.download(image, `canvas.${format}`);
    })();
  }

  /**
   * Display a PNG image.
   */
  public displayImage(content: any) {
    const data = this.clone(this.__eval_data__);
    const array = self.jsoo_runtime.caml_convert_bytes_to_array(content);
    const blob = new Blob([array], { type: "image/png" });
    this.displayBlob(blob, data);
  }
}
