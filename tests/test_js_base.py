import time


def test_all(selenium_js):
    data = selenium_js.run_basthon("var a = 1+1; a;")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 1
    result = result["result"]
    assert result["text/plain"] == "2"

    data = selenium_js.run_basthon("console.log(a);")
    assert data["stdout"] == "2\n"
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 2
    assert "result" not in result

    data = selenium_js.run_basthon("console.error('toto');")
    assert data["stdout"] == ""
    assert data["stderr"] == "toto\n"
    result = data["result"]
    assert result["execution_count"] == 3
    assert "result" not in result

    data = selenium_js.run_basthon("var obj = {foo: 'bar', foobar: 3}; obj;")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 4
    result = result["result"]
    assert result["text/plain"] == "{ foo: 'bar', foobar: 3 }"

    # test if SharedArrayBuffer can be used
    data = selenium_js.run_js("return globalThis.crossOriginIsolated;")
    assert data is True

    t0 = time.perf_counter()
    data = selenium_js.run_basthon("basthon.sleep(3);")
    t1 = time.perf_counter()
    assert abs(t1 - t0 - 3) < 0.2
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 5
    assert "result" not in result
