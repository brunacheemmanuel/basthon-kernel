import { expose } from "comlink";
import { OCamlKernelWorker } from "./worker";

declare let self: DedicatedWorkerGlobalScope;

expose(OCamlKernelWorker);
